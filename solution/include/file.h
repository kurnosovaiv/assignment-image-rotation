#ifndef FILE_OPS
#define FILE_OPS
#include <stdio.h>
#include "image.h"
#include "status.h"
#include "bmp.h"
enum read_s get_image(char* const filename, struct image* const image);
enum write_s write_image(char* const filename, struct image* const image);
#endif
