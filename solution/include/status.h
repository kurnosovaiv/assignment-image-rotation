#ifndef STATUS
#define STATUS

enum  write_s {
	WRITE_OK = 0,
	SUCCESSFUL_FILE_WRITE = 1,
	WRITE_ERROR = 2,
	FILE_WRITE_ERROR = 3
};

enum read_s {
	READ_OK = 0,
	SUCCESSFUL_FILE_READ = 1,
	READ_INVALID_HEADER = 2,
	FILE_READ_ERROR = 3
};
#endif


