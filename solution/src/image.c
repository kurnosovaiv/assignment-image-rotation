#include "image.h"

struct image create_img(size_t width, size_t height) {
	struct image img = { 0 };
	img.width = width;
	img.height = height;
	return img;
}

void delete_image(struct image* img) {
	free(img->data);
}
