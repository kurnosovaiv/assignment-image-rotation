#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>

const char* write_s_output[] = {
    [WRITE_OK] = "Content written succesfully\n",
    [WRITE_ERROR] = "An error occurred while writing image contents\n",
    [FILE_WRITE_ERROR] = "An error occurred while writing into the file\n",
    [SUCCESSFUL_FILE_WRITE] = "File successfully written\n"
};

const char* read_s_output[] = {
    [READ_OK] = "Content successfully read\n",
    [READ_INVALID_HEADER] = "Error while reading the header\n",
    [SUCCESSFUL_FILE_READ] = "File successfully read\n",
    [FILE_READ_ERROR] = "Error occured while reading the file\n"
};

void std_output(const char* s) {
    fprintf(stdout, "%s", s);
}

void std_error_output(const char* s) {
    fprintf(stderr, "%s", s);
}


int main(int argc, char** argv) {
    (void)argc; (void)argv;
    if (argc != 3) {
        fprintf(stderr, "%s", "Incorrect command format, please input images");
    }
    else {
        struct image source = { 0 };
        struct image result = { 0 };
        enum read_s read_image_s = get_image(argv[1], &source);
        if (read_image_s >= 2) {
            std_error_output(read_s_output[read_image_s]);
            return 0;
        }
        else std_output(read_s_output[read_image_s]);
        result = rotate(source);
        enum write_s write_image_s = write_image(argv[2], &result);
        if (write_image_s >= 2) {
            std_error_output(write_s_output[write_image_s]);
            return 0;
        }
        else std_output(write_s_output[write_image_s]);
        delete_image(&result);
        delete_image(&source);
    }
    return 0;
}
