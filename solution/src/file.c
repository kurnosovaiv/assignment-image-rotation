#include "file.h"

enum read_s get_image(char* const filename, struct image* const image) {
	FILE* filePointer;
	filePointer = fopen(filename, "rb");
	if (filePointer == NULL)
		return FILE_READ_ERROR;
	enum read_s read_bmp_s = from_bmp(filePointer, image);
	if (read_bmp_s != READ_OK)
		return read_bmp_s;
	fclose(filePointer);
	return SUCCESSFUL_FILE_READ;
}

enum write_s write_image(char* const filename, struct image* const image) {
	FILE* filePointer;
	filePointer = fopen(filename, "wb");
	if (filePointer == NULL)
		return FILE_WRITE_ERROR;
	enum write_s write_bmp_s = to_bmp(filePointer, image);
	if (write_bmp_s != WRITE_OK)
		return write_bmp_s;
	fclose(filePointer);
	return SUCCESSFUL_FILE_WRITE;
}
